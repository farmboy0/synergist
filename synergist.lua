
_addon.name    = 'synergist'
_addon.author  = 'farmboy0'
_addon.version = '0.1'

require 'imguidef'

------------------------------------------------------------------------------------------------
-- addon state
------------------------------------------------------------------------------------------------

local claim = 0
local can_trade = false
local recipe = ''
local pressure = 0
local impurities = 0

local claim_state = function()
	if claim == 0 then
		return 'Unclaimed'
	elseif claim == 60 then
		return 'Claimed'
	elseif claim == 10 then
		return '10 seconds'
	end
end

local element_names   = {'Fire', 'Ice', 'Wind', 'Earth', 'Thunder', 'Water', 'Light', 'Dark'}
local element_status  = {'', '', '', '', '', '', '', ''}
local element_current = {0, 0, 0, 0, 0, 0, 0, 0}
local element_target  = {0, 0, 0, 0, 0, 0, 0, 0}
local element_avail   = {0, 0, 0, 0, 0, 0, 0, 0}

------------------------------------------------------------------------------------------------
-- message handlers
------------------------------------------------------------------------------------------------

local on_claim = function()
	claim = 60
	return true
end

local on_unclaim_in_10 = function()
	claim = 10
	return true
end

local on_unclaim = function()
	claim = 0
	can_trade = false
	recipe = ''
	pressure = 0
	impurities = 0
	element_status  = {'', '', '', '', '', '', '', ''}
	element_current = {0, 0, 0, 0, 0, 0, 0, 0}
	element_target  = {0, 0, 0, 0, 0, 0, 0, 0}
	element_avail   = {0, 0, 0, 0, 0, 0, 0, 0}
	return true
end

local on_info_message = function()
-- do nothing
	return true
end

local on_recipe = function(msg)
	print('Recipe')
-- TODO
	return false -- or else client gets stuck
end

local on_feed = function(msg)
	print('On feed')
-- TODO
	return true
end

local on_furnace_state = function(msg)
	pressure = string.match(msg, 'Internal pressure: (%d+) Pz/Im')
	impurities = string.match(msg, 'Impurity ratio: (%d+)%%')
	return false
end

local update_elemental_value = function(values, index, code, msg)
	local value = string.match(msg, '\239' .. code .. '(%-?%d+)')
	if value then
		values[index] = value
	end
end

local on_current_elements = function(msg)
	update_elemental_value(element_current, 1, '\31', msg)
	update_elemental_value(element_current, 2, '\32', msg)
	update_elemental_value(element_current, 3, '\33', msg)
	update_elemental_value(element_current, 4, '\34', msg)
	update_elemental_value(element_current, 5, '\35', msg)
	update_elemental_value(element_current, 6, '\36', msg)
	update_elemental_value(element_current, 7, '\37\37', msg)
	update_elemental_value(element_current, 8, '\38', msg)
	return true
end

local on_target_elements = function(msg)
	update_elemental_value(element_target, 1, '\31', msg)
	update_elemental_value(element_target, 2, '\32', msg)
	update_elemental_value(element_target, 3, '\33', msg)
	update_elemental_value(element_target, 4, '\34', msg)
	update_elemental_value(element_target, 5, '\35', msg)
	update_elemental_value(element_target, 6, '\36', msg)
	update_elemental_value(element_target, 7, '\37\37', msg)
	update_elemental_value(element_target, 8, '\38', msg)
	return true
end

------------------------------------------------------------------------------------------------
-- ui actions
------------------------------------------------------------------------------------------------

local claim_furnace = function()
	print('Claiming furnace')
end

local twack_furnace = function()
	print('Twacking')
end

local repair_furnace = function()
	print('Repair')
end

local trade_materials = function()
	print('Trading materials')
end

local feed = function(element_index)
	print('Feeding ' .. element_names[element_index])
end

local operate_pressure_handle = function()
	print('Pressure Handle')
end

local operate_safety_lever = function()
	print('Safety lever')
end

local recycle_fewell = function()
	print('Recycle')
end

------------------------------------------------------------------------------------------------
-- Synergy messages
------------------------------------------------------------------------------------------------

local MSG_CLAIM='\30\3Synergy crucible\30\1 set\. You now have claim over the synergy furnace..Please deposit your ingredients inside the furnace and initiate the synergy process within 1 minute\.\127\49'
local MSG_CLAIM_ITEMS='Please be aware that certain items are not suitable for use as synergy ingredients\.\127\49'
local MSG_UNCLAIM_10s='Your claim over the synergy furnace will expire in ten seconds\.'
local MSG_UNCLAIM='\31\121Your claim to the synergy furnace has been relinquished\.'
local MSG_RECIPE='\30?\1?Synergy rank: (%a+)\7Objective: %d+ \30\2([%w%s]+)\30\1\7Difficulty: (.+)'
local MSG_COMMENCING='Commencing synergy process.\127\49'
local MSG_FEED='You feed the furnace 1 portion of (\239.) fewell\.\7(\239.) elemental power climbs to ([0-9]+)\.'
local MSG_TARGET_VALUE='\239. elemental power is far off the target value.'
local MSG_FURNACE_STATE='Internal pressure: (%d+) Pz/Im.Impurity ratio: (%d+)%%'
local MSG_CURRENT_ELE='\30?\1?Internal elemental balance:\7\239\31%-?%d+ \239\32%-?%d+ \239\33%-?%d+ \239\34%-?%d+ \239\35%-?%d+ \239\36%-?%d+ \239.%-?%d+ \239\38%-?%d+'
local MSG_TARGET_ELE='\31\141\239\31%d+ \239\32%d+ \239\33%d+ \239\34%d+ \239\35%d+ \239\36%d+ \239.%d+ \239\38%d+'

local messages =
{
	[MSG_CLAIM]          = on_claim,
	[MSG_CLAIM_ITEMS]    = on_info_message,
	[MSG_UNCLAIM_10s]    = on_unclaim_in_10,
	[MSG_UNCLAIM]        = on_unclaim,
	[MSG_RECIPE]         = on_recipe,
	[MSG_COMMENCING]     = on_info_message,
	[MSG_FEED]           = on_feed,
	[MSG_TARGET_VALUE]   = on_info_message,
	[MSG_FURNACE_STATE]  = on_furnace_state,
	[MSG_CURRENT_ELE]    = on_current_elements,
	[MSG_TARGET_ELE]     = on_target_elements,
}

------------------------------------------------------------------------------------------------
-- Event Handler
------------------------------------------------------------------------------------------------

ashita.register_event('load', function()
	-- TODO
	end)

ashita.register_event('render', function()
	imgui.SetNextWindowSize(450, 250, ImGuiSetCond_Always)
--	imgui.SetNextWindowSizeConstraints(450, 150, FLT_MAX, FLT_MAX)
	local flags = ImGuiWindowFlags_NoResize -- ImGuiWindowFlags_*
	local begin = imgui.Begin('Synergist', true, flags)
	if begin then

		imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'Furnace')
		imgui.SameLine()
		local pressed = imgui.Button('Claim')
		if pressed and claim == 0 then
			claim_furnace()
		end
		imgui.SameLine()
		imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'Status: ' .. claim_state())
		imgui.SameLine()
		local pressed = imgui.Button('Twack')
		if pressed then
			twack_furnace()
		end
		imgui.SameLine()
		local pressed = imgui.Button('Repair')
		if pressed then
			repair_furnace()
		end

		imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'Recipe')
		imgui.SameLine()
		local pressed = imgui.Button('Trade Materials')
		if pressed and claim > 0 and can_trade then
			trace_materials()
		end
		imgui.SameLine()
		imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'Recipe: ' .. recipe)

		imgui.BeginGroup()
			imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'Current')
			imgui.Separator()
			imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'Target')
			imgui.Separator()
			imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'fewell')
			imgui.Separator()
			imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'Feed')
		imgui.EndGroup()

		for e = 1, 8 do
			imgui.SameLine()
			imgui.BeginGroup()
				imgui.TextColored(1.0, 1.0, 1.0, 1.0, element_current[e])
				imgui.Separator()
				imgui.TextColored(1.0, 1.0, 1.0, 1.0, element_target[e])
				imgui.Separator()
				imgui.TextColored(1.0, 1.0, 1.0, 1.0, element_avail[e])
				imgui.Separator()
				imgui.PushStyleColor(ImGuiCol_Button, 0.25, 0.69, 1.0, 0.8)
				local pressed = imgui.Button(element_names[e])
				if pressed then
					feed(e)
				end
				imgui.PopStyleColor();
			imgui.EndGroup()
		end

		local pressed = imgui.Button('Recycle')
		if pressed then
			recycle_fewell()
		end

		imgui.Separator()

		imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'Pressure:')
		imgui.SameLine()
		imgui.TextColored(1.0, 1.0, 1.0, 1.0, pressure .. ' Pz/Im')
		imgui.SameLine()
		local pressed = imgui.Button('Pressure Handle')
		if pressed then
			operate_pressure_handle()
		end

		imgui.Separator()

		imgui.TextColored(1.0, 1.0, 1.0, 1.0, 'Impurity ratio:')
		imgui.SameLine()
		imgui.TextColored(1.0, 1.0, 1.0, 1.0, impurities .. ' %%')
		imgui.SameLine()
		local pressed = imgui.Button('Safety Lever')
		if pressed then
			operate_safety_lever()
		end
	end
	imgui.End()
end)

ashita.register_event('incoming_packet', function(id, size, packet, packet_modified, blocked)
	-- TODO
	return false
end)

ashita.register_event('outgoing_packet', function(id, size, packet, packet_modified, blocked)
	-- TODO
	return false
end)

ashita.register_event('incoming_text', function(mode, message, mode_mod, message_mod, blocked)
	for m, f in pairs(messages) do
		if message:match('^' .. m .. '$') then
			return f(message)
		end
	end
	return false
end)

ashita.register_event('command', function(cmd, nType)
	if not string.match(cmd, '^/synergist') then
		return false
	end

	local args = {}
	for arg in string.gmatch(cmd, '([^%s]+)') do args[#args + 1] = arg end

	-- TODO

	return true
end)
